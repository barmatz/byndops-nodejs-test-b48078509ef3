FROM node:alpine

RUN mkdir /byndops-nodejs-test-b48078509ef3
WORKDIR /byndops-nodejs-test-b48078509ef3

COPY package.json /byndops-nodejs-test-b48078509ef3
RUN npm install

COPY . /byndops-nodejs-test-b48078509ef3

EXPOSE 8080

CMD ["npm", "start"]