byndops-nodejs-test-b48078509ef3
================================

This is an API for uploading and serving images.

# Prerequisites

If you plan to run the application outside of Docker, make sure you install the following dependencies:

- [Docker](https://www.docker.com/)
- [Node.js (with ES6 support)](https://nodejs.org/)
- [MongoDB](https://www.mongodb.com/)

# Run

Using [Docker Compose](https://docs.docker.com/compose/) you can run the application in its Docker container.
Simply run:
```
docker-compose up
```

__Note:__ To run locally you will need to make MongoDB avaiable for the application, install the Node dependencies (`npm install`), and run `npm start`.

## API methods

### GET /images

Returns a list of all image objects.

Example:
```
curl -i \
  http://localhost:8080/v1/images
```

### GET /images/{id}

Returns an image object by `id`.

Example:
```
curl -i \
  http://localhost:8080/v1/images/0
```

### POST /upload

Uploads an image.

__Note:__ set the request `Content-Type` to `multipart/form-data`.

#### Parameters
`String` __fileName__ - the image name (not to be confused with the file name).
`File` __fileData__ - the image file

Example:
```
curl -i \
  -X POST \
  -H "Content-Type: multipart/form-data" \
  -F "fileName=image" \
  -F "fileData=@./test-image.jpg" \
  http://foo:bar@localhost:8080/v1/upload
```

### GET /resize/{id}

Resizes an image by `id`.

Example:
```
curl -i \
  'http://localhost:8080/v1/resize/0?width=100&height=80'
```

### DELETE /images/{id}

Deletes an image by `id`.

Example:
```
curl -i \
  -X DELETE \
  http://foo:bar@localhost:8080/v1/images/0
```