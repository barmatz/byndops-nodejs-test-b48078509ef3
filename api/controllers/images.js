const Image = require('../models/image');

function listImages(req, res) {
  Image
    .find()
    .then((images) => res.json(images))
    .catch((err) => res.status(500).json({ message: err }));
}

module.exports = { listImages };