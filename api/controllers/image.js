const config = require('config')
    , path = require('path')
    , fs = require('fs')
    , sizeOf = require('image-size')
    , bluebird = require('bluebird')
    , jimp = require('jimp')
    , Image = require('../models/image')
    , logger = require('../../logger')
    , unlinkFile = bluebird.promisify(fs.unlink)
    , HOST = config.get('server.host');

function findImageById(req, res) {
  return Image
    .findOne({ id: req.swagger.params.id.value })
    .exec();
}

function resolveImagePath(filePath) {
  return path.resolve(__dirname, '..', '..', filePath);
}

function throwImageNotFoundError() {
  let err = new Error('Image object not found');
  err.status = 404;
  throw err;
}

function catchAndRespond(res, promise) {
  return promise.catch((err) => {
    let message = err.message || err
      , status = err.status || 500;

    logger.warn(message);
    res.status(status).json({ message });
  });
}

function createNewImage(id, fileName, filePath, meta, save = true) {
  let width = meta.width
    , height = meta.height
    , imageDimensions = isNaN(width) && isNaN(height) ? sizeOf(resolveImagePath(filePath)) : { width, height }
    , image = new Image({
        id: id,
        name: fileName,
        height: imageDimensions.width,
        width: imageDimensions.height,
        url: `${HOST}/${filePath}`,
        meta: meta
      });

  return save ? image.save() : image;
}

function uploadImage(req, res) {
  let file = req.files.fileData[0]
    , path = file.path;

  catchAndRespond(res, 
    Image
      .count()
      .then((count) => createNewImage(count++, req.body.fileName, path, file))
      .then((image) => res.json(image))
  );
}

function getImage(req, res) {
  catchAndRespond(res, 
    findImageById(req, res)
      .then((image) => {
        if (image) {
          res.json(image);
        } else {
          throwImageNotFoundError();
        }
      })
  );
}

function deleteImage(req, res) {
  let imagePath;

  function deleteFile(filePath) {
    return unlinkFile(filePath).catch((err) => err);
  }

  catchAndRespond(res, 
    findImageById(req, res)
      .then((image) => {
        if (!image) {
          throwImageNotFoundError();
        } else {
          imagePath = resolveImagePath(image.meta.path);

          return image; 
        }
      })
      .then((image) => {
        let children = image.meta.children;

        if (children) {
          return bluebird.all(children.map((filePath) => deleteFile(filePath))).then(() => image);
        } else {
          return image;
        }
      })
      .then((image) => deleteFile(imagePath).then(() => image).catch(() => image))
      .then((image) => image.remove())
      .then(() => res.json({ message: 'Image object deleted' }))
  );
}

function resizeImage(req, res) {
  let params = req.swagger.params
    , width = params.width.value
    , height = params.height.value
    , name
    , destination
    , fileName
    , filePath
    , extension
    , originalImage
    , imageData
    , newFilePath;

  catchAndRespond(res, 
    findImageById(req, res)
      .then((image) => {
        if (image) {
          originalImage = image;

          imageData = image.meta;
          imageData.width = width;
          imageData.height = height;

          name = image.name;
          destination = imageData.destination;
          fileName = imageData.filename;
          filePath = path.join(destination, fileName);
          extension = path.extname(fileName);
          
          fileName = `${path.basename(fileName, extension)}_resized_${width}x${height}`;
          newFilePath = path.join(destination, `${fileName}${extension}`);
        } else {
          throwImageNotFoundError();
        }
      })
      .then((image) => jimp.read(resolveImagePath(filePath)))
      .then((image) => image.resize(width, height))
      .then((image) => image.write(newFilePath))
      .then((image) => {
        let children = imageData.children || {};

        if (children.indexOf(newFilePath) === -1) {
          children.push(newFilePath);
        }

        return originalImage.set('meta.children', children).save().then(() => image);
      })
      .then((image) => bluebird.promisify(image.getBuffer, { context: image })(image.getMIME()))
      .then((image) => createNewImage(originalImage.id, name, newFilePath, imageData, false))
      .then((image) => res.json(image))
  );
}

module.exports = { uploadImage, getImage, deleteImage, resizeImage };