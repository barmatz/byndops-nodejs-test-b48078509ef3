const mongoose = require('mongoose');

module.exports = mongoose.model('Image', {
  id: Number,
  name: String,
  height: Number,
  width: Number,
  url: String,
  meta: {
    size: Number,
    path: String,
    filename: String,
    destination: String,
    mimetype: String,
    encoding: String,
    originalname: String,
    fieldname: String,
    children: Array
  }
});