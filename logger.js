const winston = require('winston')
    , NODE_ENV = process.env.NODE_ENV;

let level;

switch (NODE_ENV) {
  default:
    level = 'info';
    break;
  case 'development':
    level = 'debug';
    break;
  case 'test':
    level = 'error';
    break;
}

module.exports = new winston.Logger({
  transports: [
    new winston.transports.Console({ colorize: true, level })
  ]
});