const auth = require('basic-auth')
    , config = require('config')
    , chalk = require('chalk')
    , logger = require('./logger');

module.exports = (req, authOrSecDef, scopesOrApiKey, callback) => {
  let user = auth(req)
    , username = user && user.name
    , password = user && user.pass
    , err;

  logger.debug(`Authenticating request (username: ${chalk.cyan(username)}, pass: ${chalk.cyan(password)})`);

  if (!user || !username || !password) {
    logger.debug('Missing username or password');
    err = 'Missing username or password';
  } else  if(username !== config.get('server.security.basic.user') || password !== config.get('server.security.basic.pass')) {
    logger.debug('Invalid username or password');
    err = 'Invalid username or password';
  } else {
  }

  if (err) {
    req.res.status(401).json({ message: err });
  } else {
    logger.debug('User authenticated');
    callback();
  }
}