const SwaggerExpress = require('swagger-express-mw')
    , config = require('config')
    , path = require('path')
    , express = require('express')
    , morgan = require('morgan')
    , multer = require('multer')
    , chalk = require('chalk')
    , db = require('./db')
    , logger = require('./logger')
    , basicAuth = require('./basic-auth')
    , ENV = process.env
    , NODE_ENV = ENV.NODE_ENV
    , PORT = ENV.PORT || config.get('server.port')
    , UPLOADS_DESTINATION = config.get('server.uploadsPath');

let app = express()
  , uploadStorage = multer.diskStorage({
      destination: (req, file, done) => done(null, UPLOADS_DESTINATION),
      filename: (req, file, done) => done(null, `${(new Date()).getTime()}_${file.originalname}`)
    })
  , upload = multer({
      storage: uploadStorage
    })
  , serverConfig = {
      appRoot: __dirname,
      swaggerSecurityHandlers: {
        basicAuth: basicAuth
      }
    }
  , server

SwaggerExpress.create(serverConfig, (err, swaggerExpress) => {
  if (err) {
    logger.error(err);
    throw err;
  }

  switch (NODE_ENV) {
    default:
      app.use(morgan('common'));
      break;
    case 'development':
      logger.warn('Server running in development mode');
      app.use(morgan('dev'));
      break;
    case 'test':
      logger.warn('Server running in test mode');
      break;
  };

  app
    .use(upload.fields([{ name: 'fileData' }]))
    .use(`/${UPLOADS_DESTINATION}`, express.static(UPLOADS_DESTINATION));

  swaggerExpress.register(app);

  server = app.listen(PORT, () => {
    let address = server.address()
      , serverPort = app.serverPort = address.port
      , serverAddress = app.serverAddress = address.address;

    logger.info(`Server running on port ${chalk.cyan(`${serverAddress}:${serverPort}`)}`);
  });
});

module.exports = app;
