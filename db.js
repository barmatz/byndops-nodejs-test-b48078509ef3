const mongoose = require('mongoose')
    , bluebird = require('bluebird')
    , config = require('config')
    , chalk = require('chalk')
    , logger = require('./logger')
    , DB_URI = `mongodb://${config.get('mongodb.host')}:${config.get('mongodb.port')}`;

function onError(err) {
  logger.error(err);
}

mongoose.Promise = bluebird;

mongoose
  .connect(DB_URI)
  .catch(onError)

mongoose.connection
  .on('error', onError)
  .on('connected', () => logger.debug(`Database connected to ${chalk.cyan(DB_URI)}`))
  .on('disconnected', () => logger.debug('Database disconnected'))
  .on('open', () => logger.debug('Database connection open'));

